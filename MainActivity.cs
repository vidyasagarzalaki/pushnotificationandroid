﻿using System;
using Android.App;
using Android.Content;
using Android.Widget;
using Android.OS;

namespace NotificationDemo
{
	[Activity (Label = "NotificationDemo", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity :Activity
	{
		

		protected override void OnCreate ( Bundle bundle )
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			// Get our button from the layout resource,
			// and attach an event to it
			Button button = FindViewById<Button> (Resource.Id.myButton);
			
			button.Click += delegate
			{
					StartAlarm();

			};
		}


		void StartAlarm()
		{
			Intent alarmIntent = new Intent(this, typeof(HomeActivity));
			PendingIntent pendingIntent = PendingIntent.GetBroadcast(this, 0, alarmIntent, 0);

			AlarmManager manager = (AlarmManager) GetSystemService(Context.AlarmService);
			//const int interval = 100000;

			DateTime dtSystemTime = DateTime.Now;
			DateTime dt = new DateTime (2015,5,12,17,30,0);

			long seconds =(long) (dt - dtSystemTime).TotalMilliseconds;

			//Console.WriteLine (dt + 18 * 60 * 10000);
			long intRealTime= SystemClock.ElapsedRealtime()+seconds;

			//manager.SetInexactRepeating(AlarmType.RtcWakeup,DateTime.Now.Millisecond,interval, pendingIntent);
			//manager.Set(AlarmType.RtcWakeup,interval,pendingIntent);
			//long test=interval+intRealTime;


			manager.Set(AlarmType.ElapsedRealtimeWakeup,intRealTime, pendingIntent);

		}
	}
}