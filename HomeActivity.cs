﻿using System;

using Android.Content;
using Android.Widget;
using Android.App;
using Android.OS;

namespace NotificationDemo
{
	[BroadcastReceiver]		
	public class HomeActivity :BroadcastReceiver
	{
		public override  void OnReceive ( Context context , Intent intent )
		{
			
			Toast.MakeText (context, "Service call has been updated", ToastLength.Short).Show ();
			Console.WriteLine ("triggered");
			fnNotification (context);
		}

		void fnNotification(Context context)
		{
			Intent alarmIntent = new Intent(context, typeof(NotificationClicked));
			PendingIntent pendingIntent = PendingIntent.GetActivity(context, 0, alarmIntent, 0);


			Notification.Builder objBuilder = new Notification.Builder(context);
			objBuilder.SetContentTitle ("Notification");
			objBuilder.SetContentText ("The button has been clicked");
			objBuilder.SetAutoCancel (true);
			objBuilder.SetContentIntent (pendingIntent);
			objBuilder.SetShowWhen (true);
			objBuilder.SetWhen (Java.Lang.JavaSystem.CurrentTimeMillis());
			objBuilder.SetSmallIcon (Resource.Drawable.Icon);
			objBuilder.SetSound ();
			Notification notification = objBuilder.Build ();
			NotificationManager objNotificationManager= (NotificationManager)context.GetSystemService (Context.NotificationService) as NotificationManager;
			objNotificationManager.Notify (0, notification);
		}
	}
}

